require "spec_helper"

describe Passkit do
  it "has a version number" do
    expect(Passkit::VERSION).not_to be nil
  end

  it "has IMAGE_SIZE" do
    expect(Passkit::IMAGE_SIZE).not_to be nil
  end

  it "has error class" do
    expect(PasskitError).not_to be nil
  end

  it "has error class" do
    expect(PasskitV2::PasskitError).not_to be nil
  end

  it "can debug" do
    PKLogger.debug("test")
  end

  it "can initialize" do
    p = PasskitV2.new("https://api-pass.passkit.net","helETsZJXduYxXLrGbTONCixDLlqFvTX","OvE5R9sJM9t3pP6ruvjj1aHEK32Uq799fG1oO8tNjANNR79lIU8YFHvWlki6ogf8GcG13cCZRQQbdk2syw2PZb3b7OIgluXwNkIiiX7uP4kA0LLw14OispgZnSKl8xBZ")
  end

  it "can fetch subusers" do
    p = PasskitV2.new("https://api-pass.passkit.net","helETsZJXduYxXLrGbTONCixDLlqFvTX","OvE5R9sJM9t3pP6ruvjj1aHEK32Uq799fG1oO8tNjANNR79lIU8YFHvWlki6ogf8GcG13cCZRQQbdk2syw2PZb3b7OIgluXwNkIiiX7uP4kA0LLw14OispgZnSKl8xBZ")
    resp = p.pk_subusers
    expect(resp).not_to be nil
  end

end
