require 'jwt'

module Passkit
  def self.encrypt_jwt(payload, key, signature=nil, url=nil, method=nil)
    payload[:exp] = Time.now.to_i + 60
    payload[:iat] = Time.now.to_i
    payload[:url] = url if !url.nil?
    payload[:method] = method.to_s.upcase if !method.nil?
    payload[:signature] = signature if !signature.nil?
    token = JWT.encode(payload, key, 'HS256')
    token
  end
end
