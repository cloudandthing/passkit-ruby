class PasskitError < StandardError
  ERROR_LIST = {
    "Unknown" => 100,
    "Your account pass volume is run out. Please make sure you have enough quota to issue a pass." => 101
  }
  def initialize(code,message)
    @code = code
    @message = message
  end
  def error_code
    @code
  end
  def error_message
    @message
  end
  def to_s
    s = "PK > #{@code}: #{@message}"
    Rails.logger.error s.bg_red
    s
  end
  def pk_error_code
    ERROR_LIST[@message] || 100
  end
end

class PasskitV2
  class PasskitError < PasskitError
  end
end
