module Passkit
  IMAGE_SIZE = {
    "passbook-IconFile" => "120x120>",
    "passbook-ThumbFile" => "540x540>",
    "passbook-StripFile" => "562x167>",
    "passbook-LogoFile" => "500x100>"
  }
end
