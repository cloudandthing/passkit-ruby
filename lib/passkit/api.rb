class PasskitV2
  def pk_template(template_name)
    response_json = self.pk_request(:get, "/v2/templates/#{template_name}")
    response_json
  end

  def pk_create_client
    response_json = self.pk_request(:post, "/v2/clients",{})
    response_json
  end

  def pk_user_me
    response_json = self.pk_request(:get, "/v3/users/me", {})
    response_json
  end

  def pk_subusers
    response_json = self.pk_request(:get, "/v3/subusers", {})
    response_json
  end

  def pk_subuser_transfer_quota(subuser_uid,amount)
    params = {type: "issue_quota", amount: amount}
    response_json = self.pk_request(:put, "v3/subusers/#{subuser_uid}/transferQuota", params)
    response_json
  end

  def pk_create_subuser(name)
    response_json = self.pk_request(:post, "/v3/subusers",{name: name})
    response_json
  end

  def pk_create_template(params)
    params["endDate"] = nil
    response_json = self.pk_request(:post, "/v2/templates",params)
    response_json
  end
  def pk_push_template(template_name)
    response_json = self.pk_request(:put, "/v2/templates/#{template_name}/push")
    response_json
  end
  def pk_pass(pass_id)
    return if pass_id.nil? || !pass_id.is_a?(String) || pass_id.blank?
    response_json = self.pk_request(:get, "/v2/passes/#{pass_id}")
    response_json
  end
  def pk_create_pass(pk_user_template_name, info_pk_pass)
    info_pk_pass ||= {}
    info_pk_pass[:templateName] = pk_user_template_name
    response_json = self.pk_request(:post, "/v2/passes",info_pk_pass)
    # response_json[:url] = "https://api-pass.passkit.net/v2/passes/#{response_json[:id]}/bundle"
    response_json[:url] = "https://q.passkit.net/p-#{response_json[:id]}"
    response_json
  end
  def pk_update_pass(pass_id, pk_user_template_name, info_pk_pass)
    return if pass_id.nil? || !pass_id.is_a?(String) || pass_id.blank?
    info_pk_pass ||= {}
    info_pk_pass[:templateName] = pk_user_template_name
    response_json = self.pk_request(:put, "/v2/passes/#{pass_id}",info_pk_pass)
    response_json
  end
  def pk_search_pass(template_name,size=100,start=0)
    return if template_name.nil? || template_name.blank?
    json = {size: size}
    json[:from] = start if start > 0
    json[:passFilter] = {}
    json[:passFilter][:templateName] = template_name
    self.pk_search_request(:post,"/v1/passes",json)
  end
  def pk_upload_image(filepath,url=true)
    filepath_hash = {
      key: "image",
      filepath: filepath,
      url: url
    }
    response_json = self.pk_upload('v2/images',[filepath_hash.with_indifferent_access])
    response_json['path']
  end
  def self.pk_generate_qr(uri)
    AXQR.generate_qr(uri)
  end
  def pk_certificates
    response_json = self.pk_request(:get,"/v2/passbookCerts",{})
    response_json
  end
  def pk_certificate(certificate_id)
    self.pk_request(:get,"/v2/passbookCerts/#{certificate_id}",{})
  end
  def pk_generate_csr
    self.pk_request(:post,"/v2/passbookCsrs",{})
  end
  def pk_create_certificate(certificate_path, params)
    filepath_hash = {
      key: "passbookCER",
      filepath: certificate_path,
      url: false,
      content_type: "application/pkix-cert"
    }
    response_json = self.pk_upload('v2/passbookCerts',[filepath_hash.with_indifferent_access], params)
    response_json['id']
  end
  def pk_update_certificate(certificate_id,certificate_path,params)
    filepath_hash = {
      key: "passbookCER",
      filepath: certificate_path,
      url: false,
      content_type: "application/pkix-cert"
    }
    response_json = self.pk_upload("v2/passbookCerts/#{certificate_id}",[filepath_hash.with_indifferent_access], params, :put)
    response_json['id']
  end
  def pk_campaigns
    response_json = self.pk_request(:get,"/v2/campaigns",{})
    response_json
  end
  def pk_campaign(campaign_name)
    self.pk_request(:get,"/v2/campaigns/#{campaign_name}",{})
  end
  def pk_create_campaign(params)
    params["endDate"] = nil
    self.pk_request(:post,"/v2/campaigns",params)
  end
  def pk_update_campaign(name, params)
    params["endDate"] = nil
    self.pk_request(:put,"/v2/campaigns/#{name}",params)
  end
  def pk_templates(campaign_name)
    self.pk_request(:get, "/v2/campaigns/#{campaign_name}/templates")
  end
  def pk_update_template(template_name, params)
    params["endDate"] = nil
    self.pk_request(:put, "/v2/templates/#{template_name}", params)
  end
  def pk_update_template_with_image(template_name, filepath_hash_array, params)
    params["endDate"] = nil
    self.pk_upload("/v2/templates/#{template_name}",filepath_hash_array, params, :put, true)
  end
  def pk_create_template_with_image(filepath_hash_arr, params)
    params["endDate"] = nil
    response_json = self.pk_upload('v2/templates',filepath_hash_arr, params)
    response_json['name']
  end
end
