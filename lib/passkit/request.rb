class PasskitV2
  def pk_request(method, api, params={})
    signature = api.include?('v3')
    full_uri = URI.join(self.passkit_uri,api).to_s
    jwt = Passkit.encrypt_jwt({key: self.api_key},self.api_secret) if !signature
    jwt = Passkit.encrypt_jwt({key: self.api_key},self.api_secret,Digest::SHA256.hexdigest(params.to_json),full_uri,method) if signature
    PKLogger.debug "------------------\n#{method.to_s} >>>>>> #{full_uri}\n#{params.to_json}\n------------------"
    PKLogger.debug "#{jwt}"
    begin
      if method == :get
        res = RestClient::Request.execute(method: :get, url: full_uri, timeout: 10, headers: { Authorization: "PKAuth #{jwt}", params: params })
      elsif method == :post
        res = RestClient.post full_uri, params.to_json, :content_type => :json, :accept => :json, Authorization: "PKAuth #{jwt}"
      elsif method == :put
        res = RestClient.put full_uri, params.to_json, :content_type => :json, :accept => :json, Authorization: "PKAuth #{jwt}"
      end
    rescue RestClient::Exception => e
      PKLogger.debug "#{e.response.bg_red rescue e}"
      json = JSON.parse(e.response) rescue {"error" => e.response}
      exception = PasskitError.new(e.to_s, json["error"])
      raise exception
    end
    PKLogger.debug "#{res}"
    parse_result(res.body)
  end

  def pk_search_request(method, api, params={})
    signature = api.include?('v3')
    full_uri = URI.join("https://search.passkit.net",api).to_s
    jwt = Passkit.encrypt_jwt({key: self.api_key},self.api_secret) if !signature
    jwt = Passkit.encrypt_jwt({key: self.api_key},self.api_secret,Digest::SHA256.hexdigest(params.to_json),full_uri,method) if signature
    PKLogger.debug "------------------\n#{method.to_s} >>>>>> #{full_uri}\n#{params.to_json}\n------------------"
    PKLogger.debug "#{jwt}"
    begin
      if method == :get
        res = RestClient::Request.execute(method: :get, url: full_uri, timeout: 10, headers: { Authorization: "PKAuth #{jwt}", params: params })
      elsif method == :post
        res = RestClient.post full_uri, params.to_json, :content_type => :json, :accept => :json, Authorization: "PKAuth #{jwt}"
      elsif method == :put
        res = RestClient.put full_uri, params.to_json, :content_type => :json, :accept => :json, Authorization: "PKAuth #{jwt}"
      end
    rescue RestClient::Exception => e
      PKLogger.debug "#{e.response.bg_red rescue e}"
      json = JSON.parse(e.response) rescue {"error" => e.response}
      exception = PasskitError.new(e.to_s, json["error"])
      raise exception
    end
    PKLogger.debug "#{res}"
    parse_result(res.body)
  end

  def pk_upload(api, filepath_hash_array, params = {}, method = :post, is_template_update_api = false)
    # filepath_hash_array = [{
    #   key: STRING,
    #   filepath: filepath,
    #   url: BOOLEAN,
    #   content_type: STRING,
    #   remove: BOOLEAN
    # }]
    jwt = Passkit.encrypt_jwt({key: self.api_key},self.api_secret)
    full_uri = URI.join(self.passkit_uri,api).to_s
    PKLogger.debug "------------------\n#{self.api_key}\n#{method}:#{full_uri}\n#{filepath_hash_array}\n#{params.to_json}\n------------------"
    PKLogger.debug "#{jwt}"
    processed_filepath_hash = {}
    file_arr = []
    filepath_hash_array.each do |fph|
      if fph["remove"]
        processed_filepath_hash[fph["key"]] = nil
      else
        processed_filepath_hash[fph["key"]] = {}
        file = nil
        if fph["url"]
          file = URI.parse(fph["filepath"]).open
          content_type = fph["content_type"] || file.content_type
          file_name = File.basename(fph["filepath"])
        else
          if !Passkit::IMAGE_SIZE[fph["key"]].nil?
            file = MiniMagick::Image.new(fph["filepath"]) do |b|
              b.resize Passkit::IMAGE_SIZE[fph["key"]]
              b.auto_orient
            end
            file.write(fph["filepath"])
          end
          file = File.open(fph["filepath"])
          content_type = fph["content_type"] ||  MIME::Types.type_for(File.basename(file)).first.content_type  #Better put in content_type, MIME::Types support very basic format
          file_name = File.basename(file)
        end
        processed_filepath_hash[fph["key"]] = UploadIO.new(file, content_type, file_name)
        file_arr << file
      end
    end
    processed_filepath_hash["jsonBody"] = params.to_json unless params.blank?
    url = URI.parse(full_uri)
    if method == :post
      req = Net::HTTP::Post::Multipart.new url.path, processed_filepath_hash
    elsif method == :put
      #Only update template use this atm, so it works, may need to find other way to do this properly
      if is_template_update_api
        filepath_hash_array.each do |fph|
          processed_filepath_hash["updatedMultipart"] ||= []
          processed_filepath_hash["updatedMultipart"] << fph["key"]
        end
        processed_filepath_hash["updatedMultipart"] = processed_filepath_hash["updatedMultipart"].to_json
      end
      PKLogger.debug "#{processed_filepath_hash}".bg_blue
      req = Net::HTTP::Put::Multipart.new url.path, processed_filepath_hash
    end
    req.add_field 'Authorization', "PKAuth #{jwt}"
    net = Net::HTTP.new(url.host, url.port)
    net.use_ssl = true
    #Comment this out after finish debuging
    net.set_debug_output($stdout)
    res = net.start do |http|
      http.request(req)
    end
    file_arr.map{|f|f.close}
    if res.code.to_i != 200
      PKLogger.debug "#{res.body rescue res}".bg_red
      json = JSON.parse(res.body) rescue {"error" => res.body}
      exception = PasskitError.new(res.code, json["error"])
      raise exception
    else
      PKLogger.debug "#{res.body}"
    end
    parse_result(res.body)
  end
  private
  def parse_result(input)
    result = JSON.parse(input) rescue input
    result = result.with_indifferent_access if result.is_a?(Hash)
    result
  end
end
