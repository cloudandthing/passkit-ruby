class PasskitV2
  attr_accessor :passkit_uri
  attr_accessor :api_key
  attr_accessor :api_secret

  def self.new_rp
    raise "Passkit Reseller API Key is NIL" if Settings.cloudandthing_passkit_api_key.blank? || Settings.cloudandthing_passkit_api_secret.blank?
    self.new("https://api-pass.passkit.net",Settings.cloudandthing_passkit_api_key,Settings.cloudandthing_passkit_api_secret)
  end

  def initialize(passkit_uri, api_key, api_secret)
    self.passkit_uri = passkit_uri
    self.api_key = api_key
    self.api_secret = api_secret
    self
  end
end
