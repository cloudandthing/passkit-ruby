module PKLogger
  def self.debug(s)
    s = "#{s}" if !s.is_a?(String)
    if defined?(Rails)
      Rails.logger.debug(s)
    else
      puts s
    end
  end
end
