# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'passkit/version'

Gem::Specification.new do |spec|
  spec.name          = "passkit"
  spec.version       = Passkit::VERSION
  spec.authors       = ["sakko"]
  spec.email         = ["saklism@gmail.com"]

  spec.summary       = "Passkit Connector"
  spec.homepage      = "https://bitbucket.org/cloudandthing/passkit-ruby"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency 'rest-client', '~> 1.8', '>= 1.8.0'
  spec.add_development_dependency 'multipart-post', '~> 0'
  spec.add_development_dependency 'certified', '~> 0'
  spec.add_development_dependency 'jwt', '~> 0'
end
